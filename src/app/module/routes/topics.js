'use strict';

angular.module('angularForward.topics')
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('topics', {
        url: '/topics',
        templateUrl: 'app/module/views/topics.html',
        controller: 'TopicsController'
      });

    $urlRouterProvider.otherwise('/');
  })
;