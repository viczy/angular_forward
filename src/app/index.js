'use strict';

angular.module('angularForward', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'ui.bootstrap'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });

      // .state('topics', {
      //   url: '/topics'.
      //   templateUrl: 'app/module/views/topics.html'
      //   controller: 'TopicsController'
      // });

    $urlRouterProvider.otherwise('/');
  })
;
